#!/usr/bin/env bash

choices=("Shutdown" "Restart" "Suspend" "Exit")

choice=$(printf "%s\n" "${choices[@]}" | dmenu -sb purple -nb '#282A36' -p "Power: " -l 20 -i -fn 11)

if [[ "$choice" ]]; then
    case $choice in
        "Shutdown")
            shutdown now
            ;;
        "Restart")
            reboot
            ;;
        "Suspend")
            systemctl suspend
            ;;
        "Exit")
           herbstclient quit
           ;;
    esac
fi
