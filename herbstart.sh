#!/bin/sh

options=("Setup Monitors" "No change to the monitors")


choice=$(printf "%s\n" "${options[@]}" | rofi -dmenu -i -l 20 -fn 11 -sb purple -nb '#282A36' -p "Please choose a monitor setup: ")

if [[ $choice == "Setup Monitors" ]]; then
  xrandr --output eDP --mode 1920x1080 --pos 0x904 --rotate normal --output DisplayPort-0 --primary --mode 1920x1080 --pos 1920x0 --rotate normal --output DisplayPort-1 --off
  xrandr --output DisplayPort-0 --mode 1920x1080 --rate 144.04
  herbstclient detect_monitors
  feh --bg-scale ~/.config/background
fi



