#!/usr/bin/env bash

setups=("1" "2")

setup=$(printf "%s\n" "${setups[@]}" | dmenu -i -l 20 -fn 11 -p "Setup: " -sb purple -nb '#282A36')

[[ $setup == "1" ]] && source /home/$USER/.config/herbstluftwm/herbstart-1.sh && exit 0

[[ $setup == "2" ]] && source /home/$USER/.config/herbstluftwm/herbstart-2.sh && exit 0
